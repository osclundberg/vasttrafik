var webpack = require('webpack');
var path = require('path')

var PRODUCTION = !!process.env.NODE_ENV;
console.log(PRODUCTION)

module.exports = {
	entry: [
		'./client/main.js'
	],
	output: {
		path: path.join('public'),
		filename: 'bundle.js',
    	publicPath: '/public/'
	},
	module: {
		loaders: [
			{
				test: /\.json$/,
				loaders: ['json-loader']
			}
		]
	},
	plugins: PRODUCTION ? [new webpack.optimize.UglifyJsPlugin({minimize: true, mangle: false})] : []
}