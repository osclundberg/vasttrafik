var config = require('./config.json');
var moment = require('moment-timezone')
var api = {
	key: config.key,
	secret: config.secret,
	url: 'https://api.vasttrafik.se/bin/rest.exe/v2'
}

var request = require('request');
 
const ORIGIN = 9021014002630000;
const DESTINATION = 9021014001950000;

function getToken(callback) {
	var headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Authorization': 'Basic ' + new Buffer(api.key + ':' + api.secret).toString('base64')
	}
	var data = {'grant_type': 'client_credentials'}

	var options = {
		headers: headers,
		url: 'https://api.vasttrafik.se/token',
		form: data
	}

	request.post(options, function(err, res, body) {
		var data = JSON.parse(body);
		callback(data.access_token)
	});
}


module.exports = {
	getTimeForArduino: getTimeForArduino,
	calculateTimeDiff: calculateTimeDiff,
	getNextForArduino: getNextForArduino,
	getNextNextForArduino: getNextNextForArduino,
	getNextBusHome: getNextBusHome,
	getNextBusToNordstan: getNextBusToNordstan,
	getDeparturesForTrip: getDeparturesForTrip,
	getToken: getToken,
	getCurrentHour: getCurrentHour,
	minutesFromNow: minutesFromNow
}

function getNextBusHome(callback) {
	var trip = {originId: DESTINATION, destinationId: ORIGIN}; 
	getNextDepartureTime(trip, function(time, date) {
		callback(date+' '+time);
	})
}

function getNextBusToNordstan(callback) {
	var trip = {originId: ORIGIN, destinationId: DESTINATION};
	getNextDepartureTime(trip, (time, date) => {
		callback(date+' '+time);
	});
}

function getTimeForArduino(callback) {
	var trip = {originId: ORIGIN, destinationId: DESTINATION};
	getNextDepartureTime(trip, function(time, date) {
		callback(date, time);
	})
}

function minutesFromNow(time) {
	var now  = moment.tz('Europe/Stockholm').format('HH:mm:ss');
	var currentTimeParts = now.split(':');
	var current = currentTimeParts[0] + ':' + currentTimeParts[1];

	var res = calculateTimeDiff(current,  time);
	return res;
}

function calculateTimeDiff(current, depature) {
	var curr = {hours: +current.split(':')[0], minutes: +current.split(':')[1] }
	var dep = {hours: +depature.split(':')[0], minutes: +depature.split(':')[1] }

	var currentInMinutes = (curr.hours * 60) + curr.minutes;
	var depatureInMinutes = (dep.hours * 60) + dep.minutes;

	return depatureInMinutes - currentInMinutes;
}

function getNextDepartureTime(trip, callback) {
	getDepartureForTrip(trip, function(time, date) {
		if (time) {
			callback(time, date);
		}
		else {
			getNextDepartureTime(callback);
		}
	})

}

function getDepartureForTrip(trip, callback) {
	getToken(function(token) {
		var options = {
			url: 'https://api.vasttrafik.se/bin/rest.exe/v2/trip?format=json&originId='+trip.originId+'&destId='+trip.destinationId,
			headers: {
				'Authorization': 'Bearer ' + token
			}
		}

		request(options, function(err, res, body) {
			var data = JSON.parse(body);
			var firstTrip = data.TripList.Trip[0];
			var time = firstTrip.Leg.Origin.rtTime;
			var date = firstTrip.Leg.Origin.rtDate;
			callback(time, date);
		});
	});
}

function getDeparturesForTrip(trip, callback) {
	getToken(function(token) {
		var options = {
			url: 'https://api.vasttrafik.se/bin/rest.exe/v2/trip?format=json&originId='+trip.originId+'&destId='+trip.destinationId,
			headers: {
				'Authorization': 'Bearer ' + token
			}
		}

		request(options, function(err, res, body) {
			var data = JSON.parse(body);
			// console.log(JSON.stringify(data, null, 2))
			var trips = data.TripList.Trip.map(function(trip) {
				var time = trip.Leg.Origin.rtTime || trip.Leg.Origin.time;
				var date = trip.Leg.Origin.rtDate || trip.Leg.Origin.date;
				return [date, time]
			});
			callback(trips)
		});
	});
}

function getStopMatchesForQuery(query) {
	getToken(function(token) {
		var options = {
			url: 'https://api.vasttrafik.se/bin/rest.exe/v2/location.name?format=json&input=' + query,
			headers: {
				'Authorization': 'Bearer ' + token
			}
		}

		request(options, function(err, res, body) {
			console.log(body)
		});
	})
}

function getNextForArduino(callback) {
	getDeparturesForTrip({originId: ORIGIN, destinationId: DESTINATION}, function(departures) {
			var next = departures[0]
			callback(next[0], next[1])
	})	
}
function getNextNextForArduino(callback) {
	getDeparturesForTrip({originId: ORIGIN, destinationId: DESTINATION}, function(departures) {
			var nextnext = departures[1]
			callback(nextnext[0], nextnext[1])
	})
}

function getCurrentHour() {
	var moment = require('moment-timezone')
	var now  = moment.tz('Europe/Stockholm').format('HH');
	return Number(now);
}
