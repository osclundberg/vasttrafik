/*
 *  This sketch sends data via HTTP GET requests to data.sparkfun.com service.
 *
 *  You need to get streamId and privateKey at data.sparkfun.com and paste them
 *  below. Or just customize this script to talk to other HTTP servers.
 *
 */

#include <ESP8266WiFi.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display

const char* ssid     = "tomat-mozarella";
const char* password = "olivolja88";

const char* host = "burmans.oscarjohnson.me";

void setup() {
  // Setup lcd
  lcd.init();
 // lcd.init();
  lcd.backlight();

  
  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

int value = 0;

String makeRequest(String url) {
  //Serial.print("connecting to ");
  //Serial.println(host);
  
  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return "";
  }
  
  //Serial.print("Requesting URL: ");
  //Serial.println(url);
  
  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return "";
    }
  }

  String line;
  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    line = client.readStringUntil('\r');
  }
 
  return line;
}

void printMessage(int a, int b) {
  lcd.setCursor(0,0);
  lcd.print("Nasta:");
  lcd.setCursor(1,0);
  lcd.print((char)225);
  lcd.setCursor(0,1);
  lcd.print("Darefter:");
  lcd.setCursor(1,1);
  lcd.print((char)225);

  // print dynamic content
  if (a < 10 && a >= 0) {
    lcd.setCursor(10,0);
    lcd.print(" ");
    lcd.setCursor(11,0);
  } else {
    lcd.setCursor(10,0);  
  }
  lcd.print(a);

  if (b < 10 && b >= 0) {
    lcd.setCursor(10,1);
    lcd.print(" ");
    lcd.setCursor(11,1);
  } else {
    lcd.setCursor(10,1);  
  }
  lcd.print(b);

  lcd.setCursor(12,0);
  lcd.print(" min");
  lcd.setCursor(12,1);
  lcd.print(" min");
}

void loop() {
  String next = makeRequest("/next"); 
  String nextnext = makeRequest("/nextnext"); 

  // Logging
  Serial.print("Nästa: ");
  Serial.print(next);
  Serial.println("");
  Serial.print("Därefter:");
  Serial.print(nextnext);
  Serial.println("");

  printMessage(next.toInt(), nextnext.toInt());

  delay(20000); // Update every 20 sec
}
