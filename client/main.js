var moment = require('moment-timezone');
//var minutesFromNow = require('../utils').minutesFromNow;

fetch('/nordstan').then((res) => {
	res.json().then((datetime)=> {
		document.querySelector('#city').innerHTML = minutesFromNow(datetime) + ' min';
		document.querySelector('#city-time').innerHTML = '('+datetime.split(' ')[1]+')';
	});
});

fetch('/home').then((res) => {
	res.json().then((datetime)=> {
		document.querySelector('#home').innerHTML = minutesFromNow(datetime) + ' min';
		document.querySelector('#home-time').innerHTML = '('+datetime.split(' ')[1]+')';
	});
});

function minutesFromNow(date) {
	var departure = moment(date, 'YYYY-MM-DD HH:mm');
	var diff = departure.diff(moment(), 'minutes');
	return diff;
}
