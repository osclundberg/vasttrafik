To install the development environment for arduino:
brew install ino

Hardware:
NodeMCU 1.0 (ESP-12E Module)
Luxorparts LCD-display 2x16 seriell I2C

Connect the following:
LCD GND -> Node GND
LCD VCC -> Node Vin
LCD SDA -> Node D2
LCD SCL -> Node D1

To upload the code the driver for the UART on the NodeMCU must be installed. Check on your NodeMCU if it says CP2102 or CH340. Depending on which you have, you need to install the corresponding UART.

I used Mac OS X El Capitan which needed a signed driver and made it work with [this driver](http://community.silabs.com/t5/Interface/OS-X-10-10-CP210x-VCP-Driver-Release-Candidate/m-p/139406#M1782). However the offical driver, which I couldn't get working but believe should also work, is [here](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers). 

Download the ESP8266 board package in the Arduino board manager.


### Resources
https://room-15.github.io/blog/2015/03/26/esp8266-at-command-reference/
https://esp8266e01.wordpress.com/2016/12/05/346/
http://www.teomaragakis.com/hardware/electronics/how-to-connect-an-esp8266-to-an-arduino-uno/
https://github.com/esp8266/at-command-set/blob/master/commands.txt
https://room-15.github.io/blog/2015/03/26/esp8266-at-command-reference/
https://dzone.com/articles/programming-the-esp8266-with-the-arduino-ide-in-3

NodeMCU driver get port to show up:
https://kig.re/2014/12/31/how-to-use-arduino-nano-mini-pro-with-CH340G-on-mac-osx-yosemite.html
https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers

http://iandrosov.github.io/IoT-NodeMCU-Heroku-Part2/

[Signed driver for UART CP2102 working on El Capitan (makes NodeMCU show up as a device)](http://community.silabs.com/t5/Interface/OS-X-10-10-CP210x-VCP-Driver-Release-Candidate/m-p/139406#M1782)


Display:
https://www.losant.com/blog/how-to-connect-lcd-esp8266-nodemcu I2C connection
https://github.com/marcoschwartz/LiquidCrystal_I2C
[How to install arduino package from github](https://github.com/Protoneer/GRBL-Arduino-Library)

https://arduino.stackexchange.com/questions/18015/weird-problem-with-lcd-1602-i2c-shield