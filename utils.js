var moment = require('moment-timezone')

function minutesFromNow(time) {
	var now  = moment.tz('Europe/Stockholm').format('HH:mm:ss');
	var currentTimeParts = now.split(':');
	var current = currentTimeParts[0] + ':' + currentTimeParts[1];

	var res = calculateTimeDiff(current,  time);
	return res;
}

function calculateTimeDiff(current, depature) {
	var curr = {hours: +current.split(':')[0], minutes: +current.split(':')[1] }
	var dep = {hours: +depature.split(':')[0], minutes: +depature.split(':')[1] }

	var currentInMinutes = (curr.hours * 60) + curr.minutes;
	var depatureInMinutes = (dep.hours * 60) + dep.minutes;


	var start = moment(current).toDate().getTime();
	var end = moment(depature).toDate().getTime();
	var timespan = end - start;
	var duration = moment(timespan);
	console.log(duration)
	
	//moment.duration(end.diff(startTime));

	return depatureInMinutes - currentInMinutes;
}

module.exports = {
	minutesFromNow: minutesFromNow,
	calculateTimeDiff: calculateTimeDiff
}