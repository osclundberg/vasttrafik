var calculateTimeDiff = require('../utils.js').calculateTimeDiff;
var getToken = require('../api.js').getToken;
var expect = require('chai').expect;

describe('Trip time', () => {
	describe('calculateTimeDiff', () => {
		it('should do a correct time diff calculation.', () => {
			var res = calculateTimeDiff('22:10:00', '22:15:00');
			expect(res).to.equal(5);
		});
		it('should do a correct time diff calculation when passing midnight.', () => {
			var res = calculateTimeDiff('23:52:00', '00:05:00');
			expect(res).to.equal(13);
		});
	});

	describe('getToken', () => {
		it('should get a token when asking for one.', (done) => {
			var res = getToken((token) => {
				expect(token.length).to.equal(32);
				done();
			});
		});
	});
});