var express = require('express');
var getTimeForArduino = require('./api.js').getTimeForArduino;
var getNextBusToNordstan = require('./api.js').getNextBusToNordstan;
var getNextBusHome = require('./api.js').getNextBusHome;
var minutesFromNow = require('./api.js').minutesFromNow;
var getNextNextForArduino = require('./api.js').getNextNextForArduino;
var getNextForArduino = require('./api.js').getNextForArduino;
var getCurrentHour = require('./api.js').getCurrentHour;

var app = express();
var port = 1111;

app.use(express.static('public'));

app.get('/next', function (req, res) {
	getNextForArduino(function(d, t) {
		res.json(minutesFromNow(t));
	})
})

app.get('/nextnext', function (req, res) {
	getNextNextForArduino(function(d, t) {
		res.json(minutesFromNow(t));
	})
})

app.get('/nordstan', function (req, res) {
	getNextBusToNordstan(function(t) {
		res.json(t);
	})
})

app.get('/home', function (req, res) {
	getNextBusHome(function(t) {
		res.json(t);
	})
})

app.get('/current-hour', function(req, res) {
	res.json(getCurrentHour())
})

app.listen(port, function() {
	console.log('Listening to localhost:' + port);
})
